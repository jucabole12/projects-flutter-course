import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {

  double _valorSlider = 100.0;
  bool _bloquearCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 50.0),
        child: Column(
          children: <Widget>[
            _crearSlider(),
            _checkBorx(),
            Expanded(
              child: _crearImagen()
            )
          ],
        )
      )
    );
  }

  Widget _crearSlider() {
    return Slider(
      activeColor: Colors.indigoAccent,
      label: 'Tamaño de la imagen',
      // divisions: 20,
      value: _valorSlider,
      min: 10.0,
      max: 400.0,
      onChanged: _bloquearCheck? null: (valor){
        setState(() => _valorSlider=valor);
      },
      // label: 'Tamaño de la imagen $_valorSlider',
    );
  }

  Widget _crearImagen() {
    return FadeInImage(
      image: NetworkImage('https://i.pinimg.com/originals/3c/d7/d9/3cd7d9e37b06574cc51264475ca9c26a.png'),
      width: _valorSlider,
      fit: BoxFit.contain,
      placeholder: AssetImage('assets/jar-loading.gif'), 
      fadeInDuration: Duration(milliseconds: 200),
    );
  }

  Widget _checkBorx() {
    // return Checkbox(
    //   value: _bloquearCheck, 
    //   onChanged: (valor) {
    //     setState(() => _bloquearCheck = valor );
    //   }
    // );

    return Container(
      child: Column(
        children: <Widget>[
          CheckboxListTile(
            title: Text('Bloquear Slider'),
            value: _bloquearCheck, 
            onChanged: (valor) {
              setState(() => _bloquearCheck = valor);
            }
          ),
          SwitchListTile(
            value: _bloquearCheck, 
            onChanged: (valor) {
              setState(() => _bloquearCheck = valor);
            }
          )
        ],
      ),
    );
  }
}